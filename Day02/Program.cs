﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Day02
{
	class Program
	{
		private const string FILE_NAME = "input.txt";
		static void Main(string[] args)
		{
			Console.WriteLine("Day 2: Inventory Management System");
			string[] labels = File.ReadAllLines(FILE_NAME);

			int twoTimesCount = 0, threeTimesCount = 0;
			foreach (var label in labels)
			{
				var uniqueChars = new HashSet<char>(label);
				bool twoTimesAdded = false, threeTimesAdded = false;
				foreach (char uniqueChar in uniqueChars)
				{
					var count = label.Count(x => x == uniqueChar);

					switch (count)
					{
						case 2:
							if (!twoTimesAdded)
							{
								twoTimesCount++;
								twoTimesAdded = true;
							}
							break;

						case 3:
							if (!threeTimesAdded)
							{
								threeTimesCount++;
								threeTimesAdded = true;
							}
							break;
					}

					if (threeTimesAdded && twoTimesAdded)
					{
						break;
					}
				}
			}

			Console.Write($"Part1: {threeTimesCount * twoTimesCount}");

			
			for (var i = 0; i < labels.Length - 1; i++)
			{
				var label1 = labels[i];
				for (var j = 1; j < labels.Length; j++)
				{
					var label2 = labels[j];
					var diffCount = 0;
					var assumedResult = string.Empty;
					for (var k = 0; k < label1.Length && diffCount < 2; k++)
					{
						if (label1[k] != label2[k])
						{
							diffCount++;
							assumedResult = label1.Remove(k, 1);
						}

						if (k == label1.Length - 1 && diffCount == 1)
						{
							Console.WriteLine($"Part2: {assumedResult}");
							j = i = labels.Length;
						}
					}
				}
			}
			
			Console.ReadLine();
		}
	}
}
