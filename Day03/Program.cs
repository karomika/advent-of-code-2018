﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day03
{
	class Program
	{
		private const string FILE_NAME = "input.txt";
		static void Main(string[] args)
		{
			Console.WriteLine("Day 3: No Matter How You Slice It");
			var lines = File.ReadAllLines(FILE_NAME);
			var claims = lines.Select(Claim.Parse).ToList();
			var maxWidth = claims.Max(x => x.Width + x.X);
			var maxHeight = claims.Max(x => x.Height + x.Y);

			var fabric = new int[maxWidth, maxHeight];

			foreach (var claim in claims)
			for (int x = 0; x < claim.Width; x++)
			for (int y = 0; y < claim.Height; y++)
				fabric[x + claim.X, y + claim.Y]++;


			var result = 0;
			for (int x = 0; x < fabric.GetLength(0); x++)
			for (int y = 0; y < fabric.GetLength(1); y++)
				if (fabric[x, y] > 1)
					result++; 

			Console.WriteLine($"Part1: {result}");


			foreach (var claim in claims)
			{
				var uniqueClaim = true;
				for (int x = 0; x < claim.Width && uniqueClaim; x++)
				for (int y = 0; y < claim.Height && uniqueClaim; y++)
					if (fabric[x + claim.X, y + claim.Y] > 1)
					{
						uniqueClaim = false;
					}

				if (uniqueClaim)
				{
					Console.WriteLine($"Part2: {claim.Id}");
				}
			}

			Console.ReadLine();

		}

		class Claim
		{
			public int Id { get; set; }
			public int X { get; set; }
			public int Y { get; set; }
			public int Width { get; set; }
			public int Height { get; set; }

			public static Claim Parse(string line)
			{
				var pattern = new []{ '@', ',', ':', 'x' };

				var fragments = Array.ConvertAll(line.Trim('#').Split(pattern), int.Parse);

				var result = new Claim
				{
					Id = fragments[0],
					X = fragments[1],
					Y = fragments[2],
					Width = fragments[3],
					Height = fragments[4],
				};

				return result;
			}
		}
	}
}
