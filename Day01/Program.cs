﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Day01
{
	class Program
	{
		private const string FILE_NAME = "input.txt";
		static void Main(string[] args)
		{
			Console.WriteLine("Day 1: Chronal Calibration");
			string[] lines = File.ReadAllLines(FILE_NAME);
			var changes = lines.Select(int.Parse).ToList();

			var frequency = changes.Sum();

			Console.WriteLine($"Combined frequency: {frequency}");

			frequency = 0;
			var seenFrequencies = new HashSet<int> { frequency };
			var loop = true;

			while (loop)
			{
				foreach (var change in changes)
				{
					frequency += change;
					if (seenFrequencies.Contains(frequency))
					{
						Console.WriteLine($"First repeating frequency: {frequency}");
						loop = false;
						break;
					}

					seenFrequencies.Add(frequency);
				}
			}

			
			Console.ReadLine();
		}
	}
}
